import 'package:flutter/material.dart';

class BlueButton extends StatelessWidget {
  final Widget child;

  const BlueButton({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Card(
          margin: const EdgeInsets.all(20),
          color: Colors.black,
          child: Container(
            padding: const EdgeInsets.all(20),
            child: child,
          ),
        )
      ],
    );
  }
}

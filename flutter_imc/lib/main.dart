import 'package:flutter/material.dart';
import './widget/button_widget.dart';

void main() {
  runApp(
    const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Home(),
    ),
  );
}

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController weightController = TextEditingController();
  TextEditingController heightController = TextEditingController();

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  String _infoText = 'Informe seus dados!';

  void _resetFields() {
    weightController.text = '';
    heightController.text = '';

    setState(() {
      _infoText = 'Informe seus dados!';
      _formKey = GlobalKey<FormState>();
    });
  }

  void _calc_imc() {
    setState(() {
      double weight = double.parse(weightController.text);
      double height = double.parse(heightController.text) / 100;

      double imc = weight / (height * height);

      if (imc < 18.5) {
        _infoText = 'Você está abaixo do peso! Parabéns! $imc';
      } else if (imc > 18.5 && imc < 24.9) {
        _infoText = 'Peso ideal! Parabéns! $imc';
      } else if (imc > 24.9 && imc < 29.9) {
        _infoText =
            'Levemente acima do peso! $imc -- Cuide melhor da sua saúde --:(';
      } else if (imc > 29.9 && imc < 34.9) {
        _infoText = 'Obesidade Grau I $imc';
      } else if (imc > 34.9 && imc < 39.9) {
        _infoText = 'Obesidade Grau II (Severa) $imc';
      } else {
        _infoText = 'Obesidade Grau III (Mórbida) $imc';
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Calculadora IMC'),
        centerTitle: true,
        backgroundColor: Colors.black,
        actions: [
          IconButton(
            icon: const Icon(Icons.refresh),
            onPressed: _resetFields,
          )
        ],
      ),
      backgroundColor: Colors.white70,
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(20),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              const BlueButton(
                child: Text(
                  'Seja bem vindo ao nosso aplicativo!',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              const Icon(Icons.person_outline, size: 50, color: Colors.black),
              TextFormField(
                validator: (value) {
                  if (value!.isEmpty) {
                    return "Insira seu peso!";
                  }
                },
                keyboardType: TextInputType.number,
                decoration: const InputDecoration(
                  labelText: 'Peso (KG)',
                  labelStyle: TextStyle(
                    color: Colors.black,
                  ),
                ),
                style: const TextStyle(color: Colors.black, fontSize: 25),
                textAlign: TextAlign.center,
                controller: weightController,
              ),
              TextFormField(
                validator: (value) {
                  if (value!.isEmpty) {
                    return "Insira sua altura!";
                  }
                },
                keyboardType: TextInputType.number,
                decoration: const InputDecoration(
                  labelText: 'Altura (cm)',
                  labelStyle: TextStyle(
                    color: Colors.black,
                  ),
                ),
                style: const TextStyle(color: Colors.black, fontSize: 25),
                textAlign: TextAlign.center,
                controller: heightController,
              ),
              const SizedBox(
                height: 50,
              ),
              SizedBox(
                height: 50,
                child: ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      _calc_imc();
                    }
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Colors.black,
                  ),
                  child: const Text(
                    'Calcular',
                    style: TextStyle(color: Colors.white, fontSize: 15),
                  ),
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Text(
                _infoText,
                textAlign: TextAlign.center,
                style: const TextStyle(
                  color: Colors.black,
                  fontSize: 25.00,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
